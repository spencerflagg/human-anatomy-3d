---
title: 'Understanding basic anatomical terms (for muscles)'
description: 'If you are looking to become a medical professional, then it is important that you learn how to understand anatomical terminology, we will quickly and easily go over the basics to get you up to speed.'
date: 2022-11-01T00:00:00Z
image: basicAnatomicalMuscularTerms/humanAnatomy3DApp_anatomicalTermsForMusclesHero.jpg
---
If you are looking to become a medical professional, then it is important that you learn how to understand anatomical terminology, which are mostly words derived from Latin and Greek. These terms can be confusing at first, but they're also precise and accurate which reduces the risk in mistaking one thing for another, which makes them very usefull for medical terms. These definitions also stay intact over time since these languages are no longer changing and evolving (also known as "dead" languages).


Anatomical terminology are often based on the structure's function, size, motion or location. In this blog we will describe many of the basic underlying termonology, once you know these basics you can see how they are used (often in combination) to specify specific parts of the human anatomy as well as making them easier to remember and understand.
# Anatomical terms of location
### Top, Bottom
**Superior (supra-)** - refers to the upper part of the body or anything that is higher up than something else. 

**Inferior (infra-, sub-)** - refers to the lower part of the body or anything that is lower down than something else. 

**Examples:**
1. *Musculus supraspinatus (supra-spinatus)*: Muscle located on the upper (superior) portion of the shoulder blade (scapula). 
2. *Musculus infraspinatus (infra-spinatus)*: Muscle located on the lower (inferior) portion of the shoulder blade (scapula). 
2. *Musculus obliquus oculi superior*: the upper muscle of the eye (oclu).
3. *Musculus obliquus inferior bulbi*: lower muscle of the eye

![hero image](/assets/images/posts/basicAnatomicalMuscularTerms/humanAnatomy3DApp_superiorInferiorMuscleAnatomicalTerms.jpg)



### Front, Back
**Anterior/Ventral** - describes the front side of the body (defined as a body standing upright with the head facing forward, arms down at the sides with the palms turned forward, and feet parallel facing forward) or anything that is located in front of something else. (The toes are anterior to the foot)

**Posterior/Dorsal** - describes the back side of the body or anything that is located behind something else. (The popliteus is posterior to the patella)

**Examples:**
1. *Musculus tibialis anterior*: Muscle located on the front (anterior) of the tibia (shinbone) 
2. *Musculus tibialis posterior*: Muscle located on the back (posterior) of the tibia (shinbone) 

![hero image](/assets/images/posts/basicAnatomicalMuscularTerms/humanAnatomy3DApp_anteriorPosteriorMuscleAnatomicalTerms.jpg)

### Left, Right 
**Lateral** - describes the sides of the body or anything that is located to the side of something else.

**Medial** -describes any structure or muscle that is close to the midline.

**Examples:**
1. *Musculus vastus medialis*: Muscle (on the thigh) located close to the midline (medial side) of the body. 
2. *Musculus vastus lateralis*: Muscle (on the thigh) located on the other side (lateral side) of the thigh.

![hero image](/assets/images/posts/basicAnatomicalMuscularTerms/humanAnatomy3DApp_vastusMedialisLateralisMuscleAnatomicalTerms.jpg)

### depth
**Superficial** - describes any structure or muscle that is closer to (superficial) the surface of the body. 

**Profundus** - describes structures that are deeply situated.

**Examples:**
1. *flexor digitorum superficialis muscle*: Muscle (on the anterior compartment of the forearm) located close(r) to the surface of the forearm in relation to the flexor digitorum profundus muscle.
2. *flexor digitorum profundus muscle*: Muscle (on the deep volar compartment of the forearm) located deep(er) in the forearm in relation to the flexor digitorum superficialis muscle.

![hero image](/assets/images/posts/basicAnatomicalMuscularTerms/humanAnatomy3DApp_digitorumProfundusSuperficialisMuscleAnatomicalTerms.jpg)

# Anatomical terms of motion
### Angular movements
**Flexion** - A bending movement that decreases the angle between two different segments. (flexing an elbow or clenching one's hand into a fist)

**Extension** - The opposite of flexion, describing a straightening movement that increases angle between body parts. (For example when standing up your knees are extended)

**Examples:**
1. *Flexor Carpi Radialis Muscle*: Muscle (on the anterior part of the forearm) used to flexes the hand at the wrist joint.
2. *Extensor Digitorum Muscle*: Muscle (on the posterior part of the forearm) used to extend the phalanges and the wrist

![hero image](/assets/images/posts/basicAnatomicalMuscularTerms/humanAnatomy3DApp_flexorExtensorCarpiRadialisMuscleAnatomicalTerms.jpg)


### motion relative to the midline
**Abduction** -  is a motion that pulls a structure or part away from the midline of the body (usually carried out by one or more abductor muscles)

**Adduction** -  is a motion that pulls a structure or part towards the midline of the body, or towards the midline of a limb

**Examples:**
1. *Adductor Longus Muscle*: Muscle (on the medial aspect of the thigh) which adducts the thigh at the hipjoint.
2. *Abductor Pollicis Brevis Muscle*: Muscle (on the lateral side in the thenar eminence of the hand) abducts the first metacarpal bone (of the thumb) at the first carpometacarpal joint,(together with the abductor pollicis longus muscle)

![hero image](/assets/images/posts/basicAnatomicalMuscularTerms/humanAnatomy3DApp_adductorAbductorMusclesAnatomicalTerms.jpg)


### up or downwards motions
**Elevation** -   is movement in a superior (upwards) direction.

**Depression** -   is movement in an inferior (downwards) direction.

**Examples:**
1. *Levator Scapulae Muscle*: Muscle (on the posterior triangle of the neck) which elevates the pectoral (shoulder) girdle at the acromioclavicular and sternoclavicular joints.
2. *Depressor Labii Inferioris Muscle*: Muscle (on in the chin area) which depresses the lower lip.

![hero image](/assets/images/posts/basicAnatomicalMuscularTerms/humanAnatomy3DApp_levatorDepressorMusclesAnatomicalTerms.jpg)


# Anatomical terms of shape and size
### size
**Brevis** -   means short

**longus** -   means long

**maximus** -   means largest.

**minor** -   means small

**minimus** -   smallest

**Examples:**
1. *Gluteus Minimus Muscle*: Muscle (deep in the posterior region of the hip) which acts as a hip stabilizer and abductor of the hip
2. *Gluteus Maximus Muscle*: Muscle (on the posterior aspect of the hip joint) which  works together with other muscles to extend the hip and to externally rotate the hip
3. *Teres Minor Muscle*: Muscle (on the posterior surface of the scapula) which produces external rotation of the shoulder joint.

![hero image](/assets/images/posts/basicAnatomicalMuscularTerms/humanAnatomy3DApp_sizeMaximusMInimusMuscleAnatomicalTerms.jpg)

### shape
**deltoid** -    means triangular

**quadratus** -   means having four sides

**rhomboideus** -   means having a rhomboid shape

**teres** -   means round or cylindrical

**trapezius** -   means having a trapezoid shape

**rectus** -   means straight

**Examples:**
1. *pronator teres Muscle*: Muscle (on the anterior aspect of the forearm) which pronates the forearm and assists in flexion of the elbow joint
2. *pronator quadratus Muscle*: Muscle (on anterior (flexor) compartment of forearm) which  produces forearm pronation by acting on the proximal radioulnar joint
3. *rectus abdominis Muscle*: Muscle (down the middle of your abdomen from your ribs to the front of your pelvis) which flexes the trunk.

![hero image](/assets/images/posts/basicAnatomicalMuscularTerms/humanAnatomy3DApp_pronatorRectorMuscleAnatomicalTerms.jpg)

# Want to learn more?
Now that you know a little more about Latin anatomical terms and how to best remember them, why not try using an anatomy learning tool to make the process even easier? VisualAnatomy3D is an interactive application that allows you to explore the anatomy of the human body in a 3D environment. With this tool, you can learn the location of different body parts and see how they are interconnected. So why not give it a try? The VisualAnatomy3D application can be downloaded for free from the [iOS App Store](https://apps.apple.com/il/app/visual-anatomy-3d-human/id1148048602), [Google Play](https://play.google.com/store/apps/details?id=com.graphicvizion.visualAnatomyHumanFr), or the [Microsoft app store](https://www.microsoft.com/en-us/p/visual-anatomy-human-body/9nblggh4qg21). 