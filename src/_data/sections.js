module.exports = [
  // { slug: 'home', name: 'Home' },
  { slug: 'info', name: 'Info', inNav: false },
  { slug: 'screenshots', name: 'Screenshots', inNav: true },
  { slug: 'systems', name: 'Systems', inNav: true },
  { slug: 'pricing', name: 'Pricing', inNav: true },
  { slug: 'download', name: 'Download', inNav: true },
  { slug: 'reviews', name: 'Reviews', inNav: true },
  { slug: 'contact', name: 'Contact', inNav: true },
  {
    slug: 'blog', name: 'Blog', inNav: true, class: 'blog', external: true,
  },
];
