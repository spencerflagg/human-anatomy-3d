module.exports = [
  {
    name: 'Mathijs Hendricks',
    quote:
      "I've tried other anatomy apps, but they were all too complex and expensive. This app, on the other hand, is affordable and straightforward. I love how easy it is to navigate and learn from.",
    stars: 5,
    site: 'App Store',
    link: '',
  },
  {
    name: 'David Miller',
    quote:
      "I use this app on a daily basis in my practice as a physical therapist. It's a great resource that helps me educate my patients and provide better care. Highly recommend!",
    stars: 5,
    site: 'App Store',
    link: '',
  },
  {
    name: 'Christopher Lowe',
    quote:
      'As someone who struggles with traditional textbook learning, this app has been a game-changer for my anatomy studies. The interactive features make it easy to visualize and understand complex concepts, which has saved me a lot of time and frustration.',
    stars: 5,
    site: 'App Store',
    link: '',
  },
];
