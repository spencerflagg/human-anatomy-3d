# Write a new Blog Post

1. Create a new file in `/src/posts/`. 
    - You can create it in the VSCode sidebar, or in the windows flie manager, or in the terminal. It doesn't matter.
    - Give the file a name that follows this convention:`yyyy-mm-dd-post-name`. 
    - Choose a file extention of either `.md` for a markdown file, or `.html` for an HTML file.
    - For example `2022-01-01-this-is-my-first-post.md`
2. Add Frontmatter to the beginning of the new file.
    - it should be surrounded by `---` and should include the following properties: `title`,`description`,`date`, and `image`.
    - `image` should correspond to the name of a file in `/src/assets/images/posts/`


# See changes quickly during development

If you run your site in development mode using the following command, your changes will appear in the browser whenever a file is saved.

```
npm run dev
```

Open http://localhost:8080 with your favorite browser to see your blog.


# Deploy changes to Production

When you are done making changes, use the following command. Your site will be built in production mode, and it will be served at a temporary local port so you can look at it in the browser.

```
npm run serve
```

Now, your blog is ready to be deployed. All generated files are located at `_site` folder, which you can deploy with any hosting service.


# Pull down Spencer's changes from the git repo

Go to the Source Control panel (Ctrl+Shift+G) and click the 🔃 Synchronize Changes button under **Source Control Repositories**.


# Push up your own changes to the git repo

1. Save your work
2. Go to the Source Control panel (Ctrl+Shift+G). You should see pending changes.
3. Type a short explanation of your change into the **message** text box.
4. Click commit.
5. Click yes (or always) to 'stage changes and commit them directly'.
6. Click 'Sync Changes'


# Glossary of Terms

## Markdown

*Markdown* is text that is styled with special characters typed within the text.
https://www.markdownguide.org/cheat-sheet/

**Example:**

```
# Heading 1
## Heading 2
_italics_
**bold**
[link text](linkurl)

```

## Markup

Markup is a system for structuring or formatting data. Notable examples include HTML (Hypertext Markup Language), XML, YAML, or KML. In web design, we mostly use the term as shorthand for HTML.

**Example of HTML:**
```
<h1>Heading 1</h1>
<h1>Heading 2</h1>
<em>italics</em>
<strong>bold</strong>
<a href="linkurl">link text</a>
```

## Frontmatter

Frontmatter is the set of properties defined at the top of a blog page. When 11ty sees the front matter block at the top of the page, it will be processed as a special file. The front matter must be the first thing in the file and must take the form of valid YAML set between triple-dashed lines.

**Example:**
```
---
title: 'abc'
description: 'xyz'
---
```

## new line
Just add \ at the end of line.


# Project structure

```
.
├── public             # Static files
│   └── assets
│       └── images     # Images not needed by Webpack
└── src
    ├── _data          # Eleventy data folder
    ├── _includes
    │   └── layouts    # HTML layout files
    ├── assets         # Assets folder that needs to be processed by Webpack
    │   ├── images
    │   │   └── posts  # Images used in your blog posts (will be compressed by Webpack)
    │   └── styles     # Your blog CSS files
    └── posts          # Your blog posts
```


# Original Template

https://github.com/ixartz/Eleventy-Starter-Boilerplate/